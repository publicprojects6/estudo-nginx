# Ver arquivos sem comentarios:
```
cat /etc/nginx/nginx.conf | grep -ve '^\s*#' | grep -ve '^$'
```
# Fazendo pull da imagem latest do Nginx
```
docker pull nginx
```

# Executar container do Nginx Web

```
docker run --name nginx-web -d --network=nginx-net -v $(pwd)/nginx-web:/etc/nginx -v $(pwd)/html:/usr/share/nginx/html -p 8080:8080 nginx
```

# Executar container do Nginx Reverse Proxy

```
docker run --name nginx-reverse-proxy -d --network=nginx-net -v $(pwd)/nginx-reverse-proxy:/etc/nginx -p 80:80 nginx
```

# Clean Docker
```
docker container stop $(docker container ls -a -q) && docker system prune -a -f --volumes
```